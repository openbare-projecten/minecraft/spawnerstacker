package nl.robertvankammen.spawnerstacker;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import lombok.var;
import org.bukkit.Location;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.bukkit.Material.SPAWNER;

public class Main extends JavaPlugin implements Listener {
    private static final int SPAWNER_COUNT = 4;
    private static final int SPAWNER_NEARBY_ENTITY = 6;
    private static final String SPAWNER_NAME = "spawner";
    private static final String PLUGIN_NAME = "mined";

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
        var config = getConfig();

        int seconds = 10;

        getServer().getScheduler().scheduleSyncDelayedTask(this, () -> loadConfigHologram(config), (seconds * 20));
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.isCancelled()) return;

        var blockAgainst = event.getBlockAgainst();
        var blockPlaced = event.getBlockPlaced();
        if (blockPlaced.getType() == SPAWNER) {
            var invetory = event.getPlayer().getInventory();
            var spawnerName = getSpawnerNameFromInvetory(invetory);

            var spawnerState = (CreatureSpawner) event.getBlockPlaced().getState();
            if (!spawnerName.equals("NO")) {
                spawnerState.setSpawnedType(EntityType.fromName(spawnerName));
                spawnerState.update();
            }
            spawnerName = spawnerState.getSpawnedType().getKey().getKey();
            getLogger().info("Spawner geplaatst met als naam " + spawnerName + " against " + locToStr(blockAgainst.getLocation()) + " placed " + locToStr(blockPlaced.getLocation()));

            if (blockAgainst.getType() == SPAWNER && blockPlaced.getType() == SPAWNER) {
                var spawnerAlreadyHere = (CreatureSpawner) blockAgainst.getState();
                var spawnerPlaced = (CreatureSpawner) blockPlaced.getState();
                if (spawnerPlaced.getSpawnedType().equals(spawnerAlreadyHere.getSpawnedType())) {
                    spawnerAlreadyHere.setSpawnCount(spawnerAlreadyHere.getSpawnCount() + SPAWNER_COUNT);
                    spawnerAlreadyHere.setMaxNearbyEntities(spawnerAlreadyHere.getMaxNearbyEntities() + SPAWNER_NEARBY_ENTITY);
                    spawnerAlreadyHere.update();
                    var spawnerType = spawnerAlreadyHere.getSpawnedType().getKey().getKey();
                    setHologram(event.getBlockAgainst().getLocation(), spawnerAlreadyHere.getSpawnCount() / SPAWNER_COUNT, spawnerType);
                    event.setCancelled(true);
                    removeFromInvetory(invetory, spawnerAlreadyHere);
                }
            }
        }
    }

    @EventHandler
    public void onBlockDestroy(BlockBreakEvent event) {
        if (event.isCancelled()) return;

        var block = event.getBlock();
        if (block.getType() == SPAWNER) {
            var spawner = (CreatureSpawner) block.getState();
            spawner.setSpawnCount(spawner.getSpawnCount() - SPAWNER_COUNT);
            spawner.setMaxNearbyEntities(spawner.getMaxNearbyEntities() - SPAWNER_NEARBY_ENTITY);
            spawner.update();
            getLogger().info("Spawner weggehaald met als naam " + spawner.getSpawnedType().getKey().getKey() + " van locatie " + locToStr(block.getLocation()));
            var location = event.getBlock().getLocation();
            var itemStack = new ItemStack(SPAWNER);
            var itemMeta = itemStack.getItemMeta();
            var spawnerType = spawner.getSpawnedType().getKey().getKey();
            itemMeta.setDisplayName(spawnerType + " " + SPAWNER_NAME + PLUGIN_NAME);
            itemStack.setItemMeta(itemMeta);
            location.getWorld().dropItemNaturally(location, itemStack);
            setHologram(location, spawner.getSpawnCount() / SPAWNER_COUNT, spawnerType);

            if (spawner.getSpawnCount() >= SPAWNER_COUNT && spawner.getMaxNearbyEntities() >= SPAWNER_NEARBY_ENTITY) {
                event.setCancelled(true);
            } else {
                var hologramLocation = location.clone();
                getConfig().set(locToStr(location), null);
                saveConfig();
                hologramLocation.add(0.5, 1.5, 0.5);
                removeHologram(hologramLocation);
            }
        }
    }

    @EventHandler
    public void noSpawnerDamage(EntityExplodeEvent blockDamageEvent) {
        var blockList = blockDamageEvent.blockList().stream()
                .filter(block -> block.getType().equals(SPAWNER))
                .collect(Collectors.toList());
        blockDamageEvent.blockList().removeAll(blockList);
    }

    @EventHandler
    public void onAnvil(PrepareAnvilEvent event) {
        var item = event.getInventory().getItem(0);
        if (item != null && item.getType().equals(SPAWNER)) {
            var location = event.getView().getPlayer().getLocation();
            location.getWorld().dropItem(location, item);
            event.getView().getPlayer().sendMessage("You cannot change your spawners name");
            event.getInventory().remove(item);
        }
    }

    private void loadConfigHologram(FileConfiguration config) {
        config.getKeys(false).stream().forEach(key -> {
            var l = (ArrayList) config.get(key, ArrayList.class);
            var locationMerged = key.split(",");
            var x = Double.parseDouble(locationMerged[0]);
            var y = Double.parseDouble(locationMerged[1]);
            var z = Double.parseDouble(locationMerged[2]);
            var world = getServer().getWorlds()
                    .stream()
                    .filter(world1 -> world1.getName().equals(locationMerged[3]))
                    .findAny().orElse(null);
            var location = new Location(world, x, y, z);

            setHologram(location, (int) l.get(0), (String) l.get(1));
        });
    }

    private void removeHologram(Location location) {
        getConfig().set(locToStr(location), null);
        saveConfig();
        HologramsAPI.getHolograms(this)
                .stream()
                .filter(h -> isLocationEqual(h.getLocation(), location))
                .collect(Collectors.toList())
                .forEach(Hologram::delete);
    }

    private void setHologram(Location location, int count, String spawnername) {
        var hologramLocation = location.clone();
        hologramLocation.add(0.5, 1.5, 0.5);
        removeHologram(hologramLocation);
        Hologram hologram = HologramsAPI.createHologram(this, hologramLocation);
        hologram.insertTextLine(0, count + " x " + spawnername);
        getConfig().set(locToStr(location), asList(count, spawnername));
        saveConfig();
    }

    static boolean isLocationEqual(Location s, Location t) {
        return locToStr(s).equals(locToStr(t));
    }

    static String locToStr(Location loc) {
        return loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + "," + loc.getWorld().getName();
    }

    private String getSpawnerNameFromInvetory(PlayerInventory inventory) {
        var itemsMainHand = inventory.getItemInMainHand();
        var itemsOffHand = inventory.getItemInOffHand();
        var response = "";
        if (itemsMainHand.getType().equals(SPAWNER)) {
            response = itemsMainHand.getItemMeta().getDisplayName().toLowerCase().replace(SPAWNER_NAME, "").trim();
        } else if (itemsOffHand.getType().equals(SPAWNER)) {
            response = itemsOffHand.getItemMeta().getDisplayName().toLowerCase().replace(SPAWNER_NAME, "").trim();
        } else {
            throw new IllegalStateException("Nooo :(");
        }

        if (response.contains(PLUGIN_NAME)) {
            return response.replace(PLUGIN_NAME, "").trim();
        } else {
            return "NO";
        }

    }

    private void removeFromInvetory(PlayerInventory inventory, CreatureSpawner spawner) {
        var itemsMainHand = inventory.getItemInMainHand();
        var itemsOffHand = inventory.getItemInOffHand();
        if (itemsMainHand.getType().equals(SPAWNER) && sameName(itemsMainHand, spawner)) {
            itemsMainHand.setAmount(itemsMainHand.getAmount() - 1);
        } else if (itemsOffHand.getType().equals(SPAWNER) && sameName(itemsOffHand, spawner)) {
            itemsOffHand.setAmount(itemsOffHand.getAmount() - 1);
        }
    }

    private boolean sameName(ItemStack itemStack, CreatureSpawner spawner) {
        return itemStack.getItemMeta().getDisplayName().contains(spawner.getSpawnedType().getKey().getKey());
    }
}
